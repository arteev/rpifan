package main

import (
	"flag"
	"fmt"

	"os"
	"rpifan/internal/rpifan/api"
	"rpifan/internal/rpifan/app"

	log "github.com/sirupsen/logrus"
)

var (
	Version   string
	DateBuild string
	GitHash   string
)

func main() {
	configFile := flag.String("config", "", "configuration file")
	version := flag.Bool("v", false, "show version")
	flag.Parse()
	if *version {
		fmt.Fprintf(os.Stdout, "rpifan version:%s build:%s hash:%s\n", Version, DateBuild, GitHash)
		return
	}

	application := app.New(*configFile, api.Version{
		Version:   Version,
		DateBuild: DateBuild,
		GitHash:   GitHash,
	})
	if err := application.Run(); err != nil {
		log.Fatal(err)
	}
}
