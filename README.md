# rpifan is a fan control service for Raspberry PI 3/4

![coverage](https://gitlab.com/arteev/rpifan/badges/master/coverage.svg?style=flat)

### Get

```shell
go get https://gitlab.com/arteev/rpifan
```

### Run

```shell
make build run
```

### Install systemd service

```shell
make build
sudo make install
sudo systemctl start rpifan
```

### Uninstall systemd service
```shell
sudo make uninstall
```

### Configuration

Configuration files are located:

- repo ./configs/config.yaml
- daemon /etc/rpifan.yaml

### Fan connection

Use a transistor as a switch to connect the fan.



