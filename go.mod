module rpifan

go 1.15

require (
	github.com/gojuno/minimock/v3 v3.0.8
	github.com/google/gofuzz v1.0.0
	github.com/google/uuid v1.1.1
	github.com/gorilla/mux v1.8.0
	github.com/rcrowley/go-metrics v0.0.0-20200313005456-10cdbea86bc0
	github.com/sirupsen/logrus v1.7.0
	github.com/spf13/afero v1.1.2
	github.com/spf13/viper v1.3.2
	github.com/stretchr/testify v1.4.0
	github.com/warthog618/gpiod v0.5.0
	golang.org/x/sys v0.0.0-20201207223542-d4d67f95c62d
)
