VERSION=$(shell git describe --tags --match '*.*.*' --always --abbrev=0)
BUILD_TIME=$(shell date -u '+%Y-%m-%d_%I:%M:%S%p')
GIT_HASH=$(shell git rev-parse HEAD)
PACKAGE=rpifan
LDFLAGS="-w -s -X main.GitHash=${GIT_HASH} -X main.DateBuild=${BUILD_TIME} -X main.Version=${VERSION}"
GOPATH=$(shell go env GOPATH)

build:
	CGO_ENABLED=0 go build -ldflags ${LDFLAGS} -o ./bin/rpifan ./cmd/rpifan/main.go

run:
	./bin/rpifan --config ./configs/config.yaml
test:
	go test ./...

generate:
	go generate ./...

.PHONY: install
install:
	go install rpifan/cmd/rpifan
	rm  /usr/bin/rpifan 2> /dev/null || true
	ln -s ${GOPATH}/bin/rpifan /usr/bin/rpifan
	cp ./scripts/rpifan.service /lib/systemd/system/rpifan.service
	cp ./configs/config.yaml /etc/rpifan.yaml
	systemctl enable rpifan

uninstall:
	systemctl stop rpifan
	systemctl disable rpifan
	rm /lib/systemd/system/rpifan.service
	rm  /usr/bin/rpifan 2> /dev/null || true

# cover calculates test coverage and removes cover files
cover: cover-calc cover-clean

# cover-calc calculates test coverage
cover-calc:
	go test -coverprofile=cover.out -coverpkg=${PACKAGE}/... ./... && \
	grep -v -E "/mocks/|_mock.go" cover.out > clear_cover.out && \
	go tool cover -func=clear_cover.out

# cover-clean removes temp cover files
cover-clean:
	rm cover.out clear_cover.out

lint:
	golangci-lint run
