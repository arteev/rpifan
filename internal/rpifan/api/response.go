package api

import (
	"fmt"
)

type contextKey uint8

const (
	requestIDKey contextKey = iota
)

type Response struct {
	Success   bool   `json:"success"`
	RequestID string `json:"request_id,omitempty"`
}

func (r *Response) SetRequestID(id string) {
	r.RequestID = id
}

type ResponseError struct {
	Response
	err          error
	ErrorMessage string `json:"error"`
}

func (e *ResponseError) Error() string {
	return e.ErrorMessage
}

func NewError(err error) *ResponseError {
	return &ResponseError{
		Response:     Response{Success: false},
		err:          err,
		ErrorMessage: err.Error(),
	}
}

func NewErrorf(format string, args ...interface{}) *ResponseError {
	return &ResponseError{
		Response:     Response{Success: false},
		ErrorMessage: fmt.Sprintf(format, args...),
	}
}

func (e *ResponseError) Unwrap() error {
	if e.err != nil {
		return e.err
	}
	return e
}
