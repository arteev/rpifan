package api

import (
	"net/http"
	"rpifan/internal/rpifan/config"
	"rpifan/pkg/fan"
	"rpifan/pkg/thermal"
)

type Fan struct {
	Type   string                 `json:"type"`
	Status string                 `json:"status"`
	Values map[string]interface{} `json:"values,omitempty"`
}

type Temperature struct {
	Zone    string `json:"zone"`
	Current uint8  `json:"current"`
	Status  string `json:"status"`
}

type Thermal struct {
	Response
	Fan         Fan         `json:"fan"`
	Temperature Temperature `json:"temperature"`
}

func ThermalHandler(fanCtl fan.Controller, tempGetter thermal.TemperatureGetter,
	cfg *config.Config) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		temperature, err := tempGetter.GetTemperature()
		status := "ok"
		if err != nil {
			status = "error"
		}
		response := &Thermal{
			Response: Response{Success: true},
			Fan: Fan{
				Type:   cfg.Fan.Controller,
				Status: fanCtl.GetStatus().String(),
				Values: fanCtl.Values(),
			},
			Temperature: Temperature{
				Current: uint8(temperature),
				Status:  status,
			},
		}
		WriteJSON(r.Context(), w, http.StatusOK, response)
	}
}

type Healthcheck struct {
	Healthy bool   `json:"healthy"`
	Reason  string `json:"reason,omitempty"`
	Meta    Meta   `json:"meta"`
}

type Version struct {
	Version   string `json:"version"`
	DateBuild string `json:"date_build"`
	GitHash   string `json:"git_hash"`
}

type Meta struct {
	Name string `json:"name"`
	Version
}

func HealthCheck(v Version) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		response := &Healthcheck{
			Healthy: true,
			Reason:  "",
			Meta: Meta{
				Name:    "rpifan",
				Version: v,
			},
		}
		WriteJSON(r.Context(), w, http.StatusOK, response)
	}
}
