package api

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"rpifan/internal/rpifan/config"
	"rpifan/mocks"
	"rpifan/pkg/fan"
	"testing"

	"github.com/gojuno/minimock/v3"
	"github.com/stretchr/testify/assert"
)

func TestThermalHandler(t *testing.T) {
	mc := minimock.NewController(t)
	defer mc.Finish()
	getterMock := mocks.NewTemperatureGetterMock(mc)
	fanMock := fan.NewControllerMock(mc)

	cfg := config.Config{
		Fan: config.Fan{Controller: "test"},
	}
	handler := ThermalHandler(fanMock, getterMock, &cfg)
	server := httptest.NewServer(handler)
	defer server.Close()

	wantTemp := 43.56
	values := map[string]interface{}{
		"v1": 1.00,
		"v2": "2",
	}
	getterMock.GetTemperatureMock.Return(wantTemp, nil)
	fanMock.GetStatusMock.Return(fan.EnabledStatus)
	fanMock.ValuesMock.Return(values)

	res, err := http.Get(server.URL)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusOK, res.StatusCode)

	response := &Thermal{}
	err = json.NewDecoder(res.Body).Decode(response)
	assert.NoError(t, err)
	assert.True(t, response.Success)
	assert.Equal(t, uint8(wantTemp), response.Temperature.Current)
	assert.Equal(t, "ok", response.Temperature.Status)
	assert.Equal(t, "test", response.Fan.Type)
	assert.Equal(t, fan.EnabledStatus.String(), response.Fan.Status)
	assert.Equal(t, values, response.Fan.Values)
}

func TestHealthCheck(t *testing.T) {
	v := Version{
		Version:   "1",
		DateBuild: "2",
		GitHash:   "3",
	}
	handler := HealthCheck(v)
	server := httptest.NewServer(handler)
	defer server.Close()

	res, err := http.Get(server.URL)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusOK, res.StatusCode)

	var response Healthcheck
	err = json.NewDecoder(res.Body).Decode(&response)
	assert.NoError(t, err)
	assert.Equal(t, response.Meta.Version, v)
	assert.True(t, response.Healthy)
	assert.Equal(t, "rpifan", response.Meta.Name)
}
