package api

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestResponse(t *testing.T) {
	wantRID := "5566abcde"
	res := &Response{}
	res.SetRequestID(wantRID)
	assert.Equal(t, wantRID, res.RequestID)
}

func TestResponseError(t *testing.T) {
	assert.Implements(t, (*error)(nil), &ResponseError{})

	// response from string
	res := NewErrorf("some error %q", "something went wrong")
	assert.False(t, res.Success)
	assert.Nil(t, res.err)
	assert.EqualError(t, res, `some error "something went wrong"`)
	assert.Equal(t, res, res.Unwrap())

	// response from error
	wantError := errors.New("test error")
	res = NewError(wantError)
	assert.False(t, res.Success)
	assert.NotNil(t, res.err)
	assert.EqualError(t, res.err, wantError.Error())
	assert.EqualError(t, res, wantError.Error())
	assert.Equal(t, wantError, res.Unwrap())

}
