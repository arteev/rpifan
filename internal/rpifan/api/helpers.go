package api

import (
	"context"
	"encoding/json"
	"net/http"

	log "github.com/sirupsen/logrus"
)

func WriteJSON(ctx context.Context, w http.ResponseWriter, status int, v interface{}) {
	w.Header().Set("Content-Type", "application/json")

	requestID := ctx.Value(requestIDKey)
	if requestID != nil {
		if are, ok := v.(interface{ SetRequestID(string) }); ok {
			are.SetRequestID(requestID.(string))
		}
	}

	data, err := json.Marshal(v)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.WriteHeader(status)
	_, err = w.Write(data)
	if err != nil {
		log.Error(err)
	}
}
