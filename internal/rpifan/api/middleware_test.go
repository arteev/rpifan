package api

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	fuzz "github.com/google/gofuzz"
	"github.com/stretchr/testify/assert"
)

func TestAuthMiddleware(t *testing.T) {
	key := "test_key_1234"
	mdw := AuthMiddleware(key)
	invoked := false
	next := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		invoked = true
	})
	handler := mdw.Middleware(next)
	w := httptest.NewRecorder()
	// authorized
	r := httptest.NewRequest("GET", "/", nil)
	r.Header.Set(headerAPIKeyName, key)
	handler.ServeHTTP(w, r)
	assert.True(t, invoked, "expected next handler")

	// unauthorized
	invoked = false
	r = httptest.NewRequest("GET", "/", nil)
	handler.ServeHTTP(w, r)
	assert.False(t, invoked, "unexpected next handler")
	assert.Equal(t, http.StatusUnauthorized, w.Code)
	response := &ResponseError{}
	err := json.Unmarshal(w.Body.Bytes(), response)
	assert.NoError(t, err)
	assert.False(t, response.Success)
	assert.Equal(t, "Unauthorized", response.ErrorMessage)
}

func TestScopeMiddleware(t *testing.T) {
	fuzz := fuzz.New().NilChance(0.5)
	const headerNameRequestID = "X-Request-Id"
	for i := 0; i < 100; i++ {
		var rID *string
		fuzz.Fuzz(rID)
		r := httptest.NewRequest("GET", "/", nil)
		if rID != nil {
			r.Header.Set(headerNameRequestID, *rID)
		}
		mdw := ScopeMiddleware()
		invoked := false
		var actualRequestID string
		next := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			invoked = true
			ctx := r.Context()
			requestValue := ctx.Value(requestIDKey)
			assert.NotNil(t, requestValue)
			actualRequestID = requestValue.(string)
			if rID == nil {
				assert.NotEmpty(t, actualRequestID)
			} else {
				assert.Equal(t, *rID, actualRequestID)
			}
		})
		handler := mdw.Middleware(next)
		w := httptest.NewRecorder()
		handler.ServeHTTP(w, r)
		assert.True(t, invoked, "expected next handler")
		gotRequestIDHeader := w.Header().Get(headerNameRequestID)
		assert.Equal(t, actualRequestID, gotRequestIDHeader)

	}
}
