package api

import (
	"context"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	fuzz "github.com/google/gofuzz"
	"github.com/stretchr/testify/assert"
)

type testValue struct {
	S string
	I int
	B bool

	RID string
}

func (t *testValue) SetRequestID(id string) {
	t.RID = id
}

func TestWriteJSON(t *testing.T) {
	fuzz := fuzz.New().NilChance(0)

	w := httptest.NewRecorder()
	wantStatus := http.StatusCreated
	var want testValue
	fuzz.Fuzz(&want)

	ctx := context.WithValue(context.Background(), requestIDKey, want.RID)

	WriteJSON(ctx, w, wantStatus, &want)
	assert.Equal(t, w.Header().Get("Content-Type"), "application/json")
	assert.Equal(t, wantStatus, w.Code)

	var got testValue
	err := json.Unmarshal(w.Body.Bytes(), &got)
	assert.NoError(t, err)
	assert.Equal(t, want, got)
}
