package api

import (
	"context"
	"net/http"

	"github.com/google/uuid"
	"github.com/gorilla/mux"
)

const headerAPIKeyName = "X-Api-Key"

// AuthMiddleware middleware authorizes the request using API key
func AuthMiddleware(apiKey string) mux.MiddlewareFunc {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			ctx := r.Context()
			key := r.Header.Get(headerAPIKeyName)
			if key != apiKey {
				err := NewErrorf("Unauthorized")
				WriteJSON(ctx, w, http.StatusUnauthorized, err)
				return
			}
			next.ServeHTTP(w, r)
		})
	}
}

// ScopeMiddleware middleware adds request data to the context
func ScopeMiddleware() mux.MiddlewareFunc {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			const header = "X-Request-Id"
			ctx := r.Context()
			requestID := r.Header.Get(header)
			if requestID == "" {
				requestID = uuid.New().String()
			}
			ctx = context.WithValue(ctx, requestIDKey, requestID)
			w.Header().Set(header, requestID)
			next.ServeHTTP(w, r.WithContext(ctx))
		})
	}
}
