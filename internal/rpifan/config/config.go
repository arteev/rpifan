package config

import (
	"fmt"
	"io"
	"os"
	"strings"

	"github.com/google/uuid"
	"github.com/spf13/viper"
)

const envPrefix = "RPIFAN"

const (
	HighLevel Level = "high"
	LowLevel  Level = "low"
)

type Config struct {
	API        API  `yaml:"api"`
	Fan        Fan  `yaml:"fan"`
	Mode       Mode `yaml:"mode"`
	customInfo []string
}

type API struct {
	Enabled bool   `mapstructure:"enabled"`
	Addr    string `mapstructure:"addr"`
	Control bool   `mapstructure:"control"`
	Key     string `mapstructure:"key"`
}

type Level string

func (l Level) String() string {
	return string(l)
}

type Mode string

const (
	Production  Mode = "production"
	Development Mode = "development"
)

type Fan struct {
	Controller    string            `mapstructure:"controller"`
	ThresholdWarn uint8             `mapstructure:"threshold_warn"`
	ThresholdOn   uint8             `mapstructure:"threshold_on"`
	ThresholdOff  uint8             `mapstructure:"threshold_off"`
	Parameters    map[string]string `mapstructure:"parameters"`
	OnLevel       Level             `mapstructure:"on_level"`
}

func (c *Config) SetCustomDefault() {
	if c.API.Enabled && c.API.Key == "" {
		c.API.Key = uuid.New().String()
		c.customInfo = append(c.customInfo, fmt.Sprintf("the api key is generated because empty: %s", c.API.Key))
	}
}

func (c *Config) Validate() error {
	if c.Fan.ThresholdOff >= c.Fan.ThresholdOn {
		return fmt.Errorf("wrong temperature threshold for switching off the fan, must be less than switching on")
	}
	if c.Fan.ThresholdWarn <= c.Fan.ThresholdOff {
		return fmt.Errorf("wrong threshold for warning, must be higher than switch-on temperature")
	}

	switch c.Fan.OnLevel {
	default:
		return fmt.Errorf("wrong level: %s", c.Fan.OnLevel)
	case HighLevel, LowLevel:
	}

	switch c.Mode {
	default:
		return fmt.Errorf("unknown mode: %s", c.Mode)
	case Production, Development:
	}

	return nil
}

func (c *Config) Print(w io.Writer) {
	fmt.Fprintf(w, "config file used: %v\n", viper.ConfigFileUsed())
	for _, s := range c.customInfo {
		fmt.Fprintf(w, "%s\n", s)
	}
}

// Load loading configuration file
func Load(configFile string) (*Config, error) {
	viper.SetDefault("fan.on_level", "high")
	viper.SetDefault("mode", "development")

	viper.SetEnvPrefix(envPrefix)
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	if configFile != "" {
		viper.SetConfigType("yaml")
		_, err := os.Stat(configFile)
		if err == nil {
			viper.SetConfigFile(configFile)
		} else {
			viper.AddConfigPath(".")
			viper.AddConfigPath("/etc/rpifan.service")
			viper.SetConfigName("config")
		}
	}
	viper.AutomaticEnv()

	if err := viper.ReadInConfig(); err != nil {
		return nil, err
	}
	config := &Config{}
	if err := viper.Unmarshal(config); err != nil {
		return nil, err
	}
	config.SetCustomDefault()
	if err := config.Validate(); err != nil {
		return nil, err
	}
	return config, nil
}
