package app

import (
	"github.com/rcrowley/go-metrics"
)

const (
	metricCPUTemp      = "cpu.temp"
	metricCPUTempMeter = "cpu.temp.meter"
	metricCPUTempWarn  = "cpu.temp.warn.counter"
	metricFanSwitchOn  = "fan.switch.on.counter"
	metricFanSwitchOff = "fan.switch.off.counter"
)

func RegisterMetrics() {
	metrics.MustRegister(metricFanSwitchOn, metrics.NewCounter())
	metrics.MustRegister(metricFanSwitchOff, metrics.NewCounter())
	metrics.MustRegister(metricCPUTempWarn, metrics.NewCounter())
	metrics.MustRegister(metricCPUTemp, metrics.NewGauge())
	metrics.MustRegister(metricCPUTempMeter, metrics.NewMeter())
}
