package app

import (
	"context"
	"rpifan/internal/rpifan/config"
	"rpifan/pkg/fan"
	"rpifan/pkg/thermal"
	"sync"
	"time"

	"github.com/rcrowley/go-metrics"
	log "github.com/sirupsen/logrus"
)

const (
	warnForceUpdateTimeout = time.Second * 5
	statisticPrintTimeout  = time.Minute * 1
	statisticSizeBuffer    = 10
)

type fanWorker struct {
	config     config.Fan
	fan        fan.Controller
	tempGetter thermal.TemperatureGetter
}

func (w *fanWorker) fanOn(t uint8) {
	if err := w.fan.On(); err != nil {
		log.Error(err)
		return
	}
	m := metrics.GetOrRegisterCounter(metricFanSwitchOn, nil)
	m.Inc(1)
	log.Debugf("fan on: t=%d", t)
}

func (w *fanWorker) fanOff(t uint8) {
	if err := w.fan.Off(); err != nil {
		log.Error(err)
		return
	}
	m := metrics.GetOrRegisterCounter(metricFanSwitchOff, nil)
	m.Inc(1)
	log.Debugf("fan off: t=%d", t)
}

func (w *fanWorker) notifyWarning(t uint8) {
	m := metrics.GetOrRegisterCounter(metricCPUTempWarn, nil)
	m.Inc(1)
	log.Warnf("t=%d", t)
}

func (w *fanWorker) statistics(t uint8) {
	m := metrics.GetOrRegisterGauge(metricCPUTemp, nil)
	m.Update(int64(t))

	meter := metrics.GetOrRegisterMeter(metricCPUTempMeter, nil)
	meter.Mark(int64(t))
}

func (w *fanWorker) Start(ctx context.Context) error {
	measures := []struct {
		name    string
		opts    []thermal.OptionMeasure
		handler func(uint8)
	}{
		{
			name: "probe-over",
			opts: []thermal.OptionMeasure{
				thermal.WithOver(w.config.ThresholdOn),
				thermal.WithNotifyCrossThreshold(true),
				thermal.WithHysteresis(1),
			},
			handler: w.fanOn,
		},
		{
			name: "probe-less",
			opts: []thermal.OptionMeasure{
				thermal.WithLess(w.config.ThresholdOff),
				thermal.WithNotifyCrossThreshold(true),
				thermal.WithHysteresis(1),
			},
			handler: w.fanOff,
		},
		{
			name: "probe-warn",
			opts: []thermal.OptionMeasure{
				thermal.WithOver(w.config.ThresholdWarn),
				thermal.WithNotifyCrossThreshold(true),
				thermal.WithHysteresis(1),
				thermal.WithForceUpdate(warnForceUpdateTimeout),
			},
			handler: w.notifyWarning,
		},
		{
			name: "probe-statistics",
			opts: []thermal.OptionMeasure{
				thermal.WithPermanently(),
				thermal.WithBuffer(statisticSizeBuffer),
			},
			handler: w.statistics,
		},
	}

	go metrics.Log(metrics.DefaultRegistry, statisticPrintTimeout, log.StandardLogger())

	wg := sync.WaitGroup{}
	wg.Add(len(measures))
	for _, m := range measures {
		m := m
		go func() {
			measure := thermal.NewMeasurer(w.tempGetter,
				m.opts...)
			defer measure.Done()
			defer wg.Done()
			for {
				select {
				case t := <-measure.C:
					m.handler(t)
				case <-ctx.Done():
					return
				}
			}
		}()
	}
	wg.Wait()
	return nil
}
