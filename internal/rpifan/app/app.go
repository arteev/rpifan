package app

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"rpifan/internal/rpifan/api"
	"rpifan/internal/rpifan/config"
	"rpifan/pkg/fan"
	"rpifan/pkg/thermal"
	"syscall"
	"time"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"github.com/warthog618/gpiod"
)

// App apllication rpifan
type App struct {
	c          config.Config
	configFile string
	version    api.Version
}

// return new Application
func New(configFile string, v api.Version) *App {
	log.SetFormatter(&log.TextFormatter{
		DisableColors: true,
		FullTimestamp: true,
	})
	log.SetOutput(os.Stdout)

	app := &App{
		configFile: configFile,
		version:    v,
	}
	return app
}

func (a *App) getSignals() chan os.Signal {
	sig := make(chan os.Signal, 1)
	signal.Notify(sig, syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM)
	return sig
}

func (a *App) controllerFactory() (fan.Controller, error) {
	switch a.c.Fan.Controller {
	default:
		return nil, fmt.Errorf("unknown controller: %s", a.c.Fan.Controller)
	case "simple", "":
		pin := a.c.Fan.Parameters["pin"]
		level := fan.LevelFromString(a.c.Fan.OnLevel.String())

		c, err := gpiod.NewChip("gpiochip0")
		if err != nil {
			return nil, err
		}

		return fan.NewFan(c, pin, level)
	case "stub":
		return fan.NewStub(), nil
	}
}

func (a *App) initLogger() {
	if a.c.Mode == config.Production {
		log.SetLevel(log.ErrorLevel)
	} else {
		log.SetLevel(log.DebugLevel)
	}
}

func (a *App) Run() error {
	RegisterMetrics()
	if err := a.loadConfig(); err != nil {
		return err
	}
	a.initLogger()

	f, err := a.controllerFactory()
	if err != nil {
		return err
	}
	defer f.Close()

	temperatureGetter, err := thermal.New(thermal.ZONE0, thermal.DefaultFS())
	if err != nil {
		return err
	}

	ctx, cancel := context.WithCancel(context.Background())
	chSignal := a.getSignals()
	defer func() {
		signal.Stop(chSignal)
	}()
	defer cancel()

	var httpServer *http.Server
	if a.c.API.Enabled {
		rounter := getRouter(f, temperatureGetter, &a.c, a.version)
		httpServer = &http.Server{
			Addr:    a.c.API.Addr,
			Handler: rounter,
		}
		go func() {
			log.Infof("Listen on %v", a.c.API.Addr)
			err := httpServer.ListenAndServe()
			if err != nil && err != http.ErrServerClosed {
				log.Fatal(err)
			}
		}()
	}

	cancelWorker := a.startWorker(ctx, f, temperatureGetter)

	for s := range chSignal {
		cancelWorker()
		if s == syscall.SIGHUP {
			log.Debug("config reload")
			if err := a.loadConfig(); err != nil {
				return err
			}
			cancelWorker = a.startWorker(ctx, f, temperatureGetter)
		} else {
			a.shutdown(httpServer)
			return nil
		}
	}

	return nil
}

func (a *App) shutdown(httpServer *http.Server) {
	if httpServer == nil {
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	if err := httpServer.Shutdown(ctx); err != nil {
		log.Error(err)
	}
}

func (a *App) loadConfig() error {
	cfg, err := config.Load(a.configFile)
	if err != nil {
		return err
	}
	cfg.Print(log.StandardLogger().Out)
	a.c = *cfg
	return nil
}

func (a *App) startWorker(ctx context.Context, fanCtl fan.Controller,
	getter thermal.TemperatureGetter) context.CancelFunc {
	ctx, cancelWorker := context.WithCancel(ctx)
	worker := &fanWorker{
		config:     a.c.Fan,
		fan:        fanCtl,
		tempGetter: getter,
	}
	go func() {
		err := worker.Start(ctx)
		if err != nil {
			log.Fatal(err)
		}
	}()
	return cancelWorker
}

func getRouter(fanCtl fan.Controller, tempGetter thermal.TemperatureGetter, cfg *config.Config,
	version api.Version) *mux.Router {
	r := mux.NewRouter()
	r.HandleFunc("/v1/api/internal/healthcheck", api.HealthCheck(version)).Methods(http.MethodGet)

	subRouter := r.PathPrefix("/v1/api").Subrouter()
	subRouter.HandleFunc("/thermal", api.ThermalHandler(fanCtl, tempGetter, cfg)).Methods(http.MethodGet)
	subRouter.Use(
		api.ScopeMiddleware(),
		api.AuthMiddleware(cfg.API.Key),
	)

	return r
}
