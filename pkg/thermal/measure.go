package thermal

import (
	"sync/atomic"
	"time"
)

type modeMeasure int

// known modes
const (
	modeUnknown modeMeasure = iota
	modeChanged
	modeOver
	modeLess
	modeSegment
	modePermanently
)

const (
	DefaultTimeout = time.Second * 2
)

type option struct {
	mode        modeMeasure
	hysteresis  uint8
	high        uint8
	low         uint8
	timeout     time.Duration
	force       time.Duration
	notifyCross bool
	bufferSize  int
}

type OptionMeasure func(o *option)

func DefaultOptions() []OptionMeasure {
	return []OptionMeasure{
		WithTimeout(DefaultTimeout),
	}
}

func WithTimeout(d time.Duration) OptionMeasure {
	return func(o *option) {
		o.timeout = d
	}
}

func WithHysteresis(g uint8) OptionMeasure {
	return func(o *option) {
		o.hysteresis = g
	}
}

func WithChanged(g uint8) OptionMeasure {
	return func(o *option) {
		o.mode = modeChanged
		o.hysteresis = g
	}
}

func WithOver(t uint8) OptionMeasure {
	return func(o *option) {
		o.mode = modeOver
		o.high = t
	}
}

func WithLess(t uint8) OptionMeasure {
	return func(o *option) {
		o.mode = modeLess
		o.low = t
	}
}

func WithNotifyCrossThreshold(b bool) OptionMeasure {
	return func(o *option) {
		o.notifyCross = b
	}
}

func WithForceUpdate(d time.Duration) OptionMeasure {
	return func(o *option) {
		o.force = d
	}
}

func WithSegment(l, h uint8) OptionMeasure {
	return func(o *option) {
		o.low = l
		o.high = h
		o.mode = modeSegment
	}
}

func WithBuffer(size int) OptionMeasure {
	return func(o *option) {
		o.bufferSize = size
	}
}

func WithPermanently() OptionMeasure {
	return func(o *option) {
		o.mode = modePermanently
	}
}

func NewMeasurer(t TemperatureGetter, opts ...OptionMeasure) *Measure {
	m := &Measure{
		o: option{
			mode: modeUnknown,
		},
		t: t,
	}
	for _, opt := range DefaultOptions() {
		opt(&m.o)
	}
	for _, opt := range opts {
		opt(&m.o)
	}

	m.C = make(chan uint8, m.o.bufferSize)
	m.done = make(chan struct{})
	go m.start()
	return m
}

type Measure struct {
	C      chan uint8
	o      option
	t      TemperatureGetter
	done   chan struct{}
	isDone int32
}

func (m *Measure) Done() {
	if atomic.CompareAndSwapInt32(&m.isDone, 0, 1) {
		close(m.C)
		close(m.done)
	}
}

func (m *Measure) start() {
	strategyProcess, err := factoryProcessing(m.o)
	if err != nil {
		panic(err)
	}

	timer := time.NewTicker(m.o.timeout)
	var chanForce <-chan time.Time
	if m.o.force > 0 {
		timerForce := time.NewTicker(m.o.force)
		chanForce = timerForce.C
	}
	for {
		select {
		case <-chanForce:
			t, err := m.t.GetTemperature()
			if err == nil && strategyProcess.MustProcessing(uint8(t)) {
				m.C <- uint8(t)
			}
		case <-timer.C:
			t, err := m.t.GetTemperature()
			if err == nil && strategyProcess.MustProcessing(uint8(t)) {
				m.C <- uint8(t)
			}
		case <-m.done:
			return
		}
	}
}
