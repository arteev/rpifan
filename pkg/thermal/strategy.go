package thermal

import "fmt"

type strategy interface {
	MustProcessing(current uint8) bool
}

type strategyFunc func(current uint8) bool

var _ strategy = (*strategyFunc)(nil)

func (fn strategyFunc) MustProcessing(current uint8) bool {
	return fn(current)
}

var factoryProcessing = func(config option) (strategy, error) {
	switch config.mode {
	default:
		return nil, fmt.Errorf("failed to factory for %v", config.mode)
	case modePermanently:
		return processPermanently(), nil
	case modeChanged:
		return processChanged(config), nil
	case modeLess:
		return processLess(config), nil
	case modeOver:
		return processOver(config), nil
	case modeSegment:
		return processSegment(config), nil
	}
}

func processPermanently() strategyFunc {
	return func(uint8) bool {
		return true
	}
}

func processChanged(o option) strategyFunc {
	var prev uint8
	return func(current uint8) bool {
		if prev == 0 || current >= prev+o.hysteresis ||
			current <= prev-o.hysteresis {
			prev = current
			return true
		}
		return false
	}
}

func processLess(o option) strategyFunc {
	prevNotify := false
	return func(current uint8) bool {
		if o.notifyCross && current > o.low+o.hysteresis {
			prevNotify = false
		}
		if current <= o.low && (!o.notifyCross || !prevNotify) {
			prevNotify = true
			return true
		}
		return false
	}
}

func processOver(o option) strategyFunc {
	prevNotify := false
	return func(current uint8) bool {
		if o.notifyCross && current < o.high-o.hysteresis {
			prevNotify = false
		}
		if current >= o.high && (!o.notifyCross || !prevNotify) {
			prevNotify = true
			return true
		}
		return false
	}
}

func processSegment(o option) strategyFunc {
	prevNotify := false
	return func(current uint8) bool {
		if o.notifyCross && (current < o.low || current > o.high) {
			prevNotify = false
		}
		if current >= o.low && current <= o.high && (!o.notifyCross || !prevNotify) {
			prevNotify = true
			return true
		}
		return false
	}
}
