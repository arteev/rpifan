package thermal

import (
	"errors"
	"path"
	"testing"

	"github.com/spf13/afero"
	"github.com/stretchr/testify/assert"
)

func writeTemperature(fs Fs, thermalZone string, temp string) error {
	filename := path.Join("/sys/class/thermal", thermalZone, "temp")
	fs.Remove(filename)
	f, err := fs.Create(filename)
	if err != nil {
		return err
	}
	defer f.Close()
	_, err = f.WriteString(temp)
	if err != nil {
		return err
	}
	return nil
}

func TestTemperature(t *testing.T) {
	fs := afero.NewMemMapFs()
	err := writeTemperature(fs, ZONE0, "30000\n")
	assert.NoError(t, err)

	getter, err := New(ZONE0, fs)
	assert.NoError(t, err)

	got, err := getter.GetTemperature()
	assert.NoError(t, err)
	assert.Equal(t, 30.0, got)

	err = writeTemperature(fs, ZONE0, "59000\n")
	assert.NoError(t, err)
	got, err = getter.GetTemperature()
	assert.NoError(t, err)
	assert.Equal(t, 59.0, got)

	err = writeTemperature(fs, ZONE0, "abcd")
	assert.NoError(t, err)
	got, err = getter.GetTemperature()
	assert.EqualError(t, errors.Unwrap(err), ErrFailedToGetTemperature.Error())

	fs = afero.NewMemMapFs()
	getter, err = New(ZONE0, fs)
	assert.EqualError(t, errors.Unwrap(err), ErrFailedToGetTemperature.Error())
}
