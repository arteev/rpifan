package thermal

import (
	"testing"
	"testing/quick"

	fuzz "github.com/google/gofuzz"
	"github.com/stretchr/testify/assert"
)

func TestStrategyFunc_MustProcessing(t *testing.T) {
	invoked := 0
	var wantCurrent uint8
	f := fuzz.New()
	f.Fuzz(&wantCurrent)

	fn := strategyFunc(func(current uint8) bool {
		assert.Equal(t, wantCurrent, current)
		invoked++
		return true
	})
	got := fn.MustProcessing(wantCurrent)
	assert.True(t, got)
	assert.NotZero(t, invoked, "not invoked")
}

func Test_processPermanently(t *testing.T) {
	if err := quick.Check(processPermanently(), nil); err != nil {
		t.Error(err)
	}
}

func Test_processChanged(t *testing.T) {
	cases := []struct {
		values   []uint8
		expected []bool
		fn       strategyFunc
		name     string
	}{
		{
			values:   []uint8{1, 2, 33, 33, 33, 2, 1, 0},
			expected: []bool{true, true, true, true, true, true, true, true},
			fn:       processPermanently(),
			name:     "processPermanently",
		},
		{
			values:   []uint8{39, 38, 40, 41, 41, 56, 55, 54, 56},
			expected: []bool{true, false, false, true, false, true, false, true, true},
			fn: processChanged(option{
				notifyCross: true,
				low:         40,
				high:        60,
				hysteresis:  2,
			}),
			name: "processChanged",
		},
		{
			values: []uint8{4, 4, 7, 50, 51, 53, 54, 50,
				49, 40, 50, 45, 54, 45},
			expected: []bool{true, false, false, false, false, false, false, true,
				false, false, false, false, false, true},
			fn: processLess(
				option{
					low:         50,
					hysteresis:  3,
					notifyCross: true,
				}),
			name: "processLess_cross",
		},
		{
			values:   []uint8{4, 49, 50, 51, 52, 50, 49},
			expected: []bool{true, true, true, false, false, true, true},
			fn: processLess(
				option{
					low:         50,
					notifyCross: false,
				}),
			name: "processLess",
		},
		{
			values: []uint8{15, 50, 55, 50, 46, 55, 40, 60,
				49, 51, 55, 45, 55},
			expected: []bool{false, true, false, false, false, false, false, true,
				false, false, false, false, true},
			fn: processOver(option{
				high:        50,
				notifyCross: true,
				hysteresis:  4,
			}),
			name: "processOver_cross",
		},
		{
			values:   []uint8{15, 49, 50, 49, 51, 51},
			expected: []bool{false, false, true, false, true, true},
			fn: processOver(option{
				high:        50,
				notifyCross: false,
			}),
			name: "processOver",
		},
		{
			values: []uint8{35, 40, 39, 40, 42,
				50, 51, 52, 49, 48},
			expected: []bool{false, true, false, true, false,
				false, false, false, true, false},
			fn: processSegment(option{
				low:         40,
				high:        50,
				notifyCross: true,
			}),
			name: "processSegment_cross",
		},
		{
			values: []uint8{35, 40, 41, 36, 45, 45,
				50, 51, 49},
			expected: []bool{false, true, true, false, true, true,
				true, false, true},
			fn: processSegment(option{
				low:         40,
				high:        50,
				notifyCross: false,
			}),
			name: "processSegment",
		},
	}
	for _, test := range cases {
		t.Run(test.name, func(t *testing.T) {
			for i, temp := range test.values {
				got := test.fn(temp)
				assert.Equal(t, test.expected[i], got, "value(%d): %v", i, temp)
			}
		})
	}
}

func Test_factoryProcessing(t *testing.T) {
	_, err := factoryProcessing(option{mode: modeUnknown})
	assert.EqualError(t, err, "failed to factory for 0")
	modes := []modeMeasure{modePermanently, modeChanged, modeLess, modeOver, modeSegment}
	for _, mode := range modes {
		s, err := factoryProcessing(option{mode: mode})
		assert.NoError(t, err)
		assert.NotNil(t, s)
	}
}
