package thermal

import (
	"errors"
	"fmt"
	"path"
	"strconv"

	"github.com/spf13/afero"
	"golang.org/x/sys/unix"
)

const (
	ZONE0 = "thermal_zone0"
)

var ErrFailedToGetTemperature = errors.New("failed to get temperature")

type Fs = afero.Fs

type Temperature struct {
	thermalZone string
	fs          Fs
}

type TemperatureGetter interface {
	GetTemperature() (float64, error)
}

var _ TemperatureGetter = (*Temperature)(nil)

var DefaultFS = afero.NewOsFs

func New(thermalZone string, fs Fs) (*Temperature, error) {
	temp := &Temperature{
		thermalZone: thermalZone,
		fs:          fs,
	}
	if _, err := temp.read(); err != nil {
		return nil, err
	}
	return temp, nil
}

func (t *Temperature) GetTemperature() (float64, error) {
	value, err := t.read()
	if err != nil {
		return 0, err
	}
	return value, nil
}

func (t *Temperature) read() (float64, error) {
	filename := path.Join("/sys/class/thermal", t.thermalZone, "temp")
	f, err := t.fs.OpenFile(filename, unix.O_CLOEXEC, unix.O_RDONLY)
	if err != nil {
		return 0, fmt.Errorf("%w: %v", ErrFailedToGetTemperature, err)
	}
	defer f.Close()

	data := make([]byte, 8)
	n, err := f.Read(data)
	if err != nil {
		return 0, fmt.Errorf("%w: %v", ErrFailedToGetTemperature, err)
	}
	temp64, err := strconv.ParseUint(string(data[:n-1]), 10, 64)
	if err != nil {
		return 0, fmt.Errorf("%w: %v", ErrFailedToGetTemperature, err)
	}
	return float64(temp64) / 1000.00, nil //nolint:gomnd
}
