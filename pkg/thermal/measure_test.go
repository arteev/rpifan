package thermal

import (
	"errors"
	"rpifan/mocks"
	"sync"
	"sync/atomic"
	"testing"
	"time"

	"github.com/gojuno/minimock/v3"
	fuzz "github.com/google/gofuzz"
	"github.com/stretchr/testify/assert"
)

func TestMeasurer(t *testing.T) {
	mc := minimock.NewController(t)
	defer mc.Finish()
	mockGetter := mocks.NewTemperatureGetterMock(t)

	invokedStrategy := int32(0)
	want := uint8(41)

	muStr := &sync.Mutex{}
	muStr.Lock()
	strategyTest := strategyFunc(func(current uint8) bool {
		assert.Equal(t, want, current)
		invokedStrategy++
		return true
	})
	muStr.Unlock()

	invokedGet := int32(0)
	mockGetter.GetTemperatureMock.Set(func() (f1 float64, err error) {
		invokedGet++
		return float64(want), nil
	})

	oldFactoryProcessing := factoryProcessing
	defer func() {
		factoryProcessing = oldFactoryProcessing
	}()
	factoryProcessing = func(config option) (strategy, error) {
		return strategyTest, nil
	}

	m := NewMeasurer(mockGetter,
		WithTimeout(time.Millisecond*100),
	)
	defer m.Done()

	assert.Equal(t, mockGetter, m.t)
	select {
	case got := <-m.C:
		assert.Equal(t, want, got)
		assert.NotZero(t, atomic.LoadInt32(&invokedStrategy))
		assert.NotZero(t, invokedGet)
	case <-time.NewTimer(time.Minute).C:
		assert.Fail(t, "failed to measure")
	}
	m.Done()

	// force
	forceTimeout := time.Millisecond * 100
	m = NewMeasurer(mockGetter,
		WithForceUpdate(forceTimeout),
		WithTimeout(time.Second*2),
	)
	defer m.Done()
	assert.Equal(t, forceTimeout, m.o.force)

	select {
	case got := <-m.C:
		assert.Equal(t, want, got)
		assert.NotZero(t, atomic.LoadInt32(&invokedStrategy))
		assert.NotZero(t, invokedGet)
	case <-time.NewTimer(time.Minute).C:
		assert.Fail(t, "failed to measure")
	}
	m.Done()

	// error: failed to get temperature
	atomic.StoreInt32(&invokedGet, 0)
	mockGetter.GetTemperatureMock.Set(func() (f1 float64, err error) {
		atomic.AddInt32(&invokedGet, 1)
		return 0, errors.New("error")
	})

	m = NewMeasurer(mockGetter,
		WithTimeout(time.Millisecond*50),
	)
	defer m.Done()
	select {
	case <-m.C:
		assert.Fail(t, "unexpected call")
	case <-time.NewTimer(time.Millisecond * 200).C:
		// ok
	}
	m.Done()

	// skip: strategy
	atomic.StoreInt32(&invokedStrategy, 0)
	want = 42
	muStr.Lock()
	strategyTest = func(current uint8) bool {
		assert.Equal(t, want, current)
		atomic.AddInt32(&invokedStrategy, 1)
		return false
	}
	muStr.Unlock()
	atomic.StoreInt32(&invokedGet, 0)
	mockGetter = mocks.NewTemperatureGetterMock(t)
	mockGetter.GetTemperatureMock.Set(func() (f1 float64, err error) {
		atomic.AddInt32(&invokedGet, 1)
		return float64(want), nil
	})

	m = NewMeasurer(mockGetter,
		WithTimeout(time.Millisecond*50),
	)
	defer m.Done()
	select {
	case <-m.C:
		assert.Fail(t, "unexpected call")
	case <-time.NewTimer(time.Millisecond * 200).C:
		// ok
	}
}

func TestDefaultOptions(t *testing.T) {
	options := DefaultOptions()
	assert.NotEmpty(t, options)
	o := &option{}
	for _, opt := range options {
		opt(o)
	}
	assert.Equal(t, DefaultTimeout, o.timeout)
}

func TestSomeWith(t *testing.T) {
	optSource := &option{}
	f := fuzz.New().NilChance(0)
	f.Fuzz(&optSource)
	optSource.notifyCross = true

	optDest := &option{}
	options := []OptionMeasure{
		WithTimeout(optSource.timeout),
		WithHysteresis(optSource.hysteresis),
		WithNotifyCrossThreshold(optSource.notifyCross),
		WithForceUpdate(optSource.force),
		WithBuffer(optSource.bufferSize),
	}

	for _, opt := range options {
		opt(optDest)
	}
	assert.Equal(t, optSource.timeout, optDest.timeout)
	assert.Equal(t, optSource.hysteresis, optDest.hysteresis)
	assert.Equal(t, optSource.notifyCross, optDest.notifyCross)
	assert.Equal(t, optSource.force, optDest.force)
}

func TestWithLess(t *testing.T) {
	o := &option{}
	want := uint8(31)
	WithLess(want)(o)
	assert.Equal(t, want, o.low)
	assert.Equal(t, modeLess, o.mode)
}

func TestWithOver(t *testing.T) {
	o := &option{}
	want := uint8(51)
	WithOver(want)(o)
	assert.Equal(t, want, o.high)
	assert.Equal(t, modeOver, o.mode)
}

func TestWithSegment(t *testing.T) {
	o := &option{}
	wantl, wanth := uint8(41), uint8(51)
	WithSegment(wantl, wanth)(o)
	assert.Equal(t, wantl, o.low)
	assert.Equal(t, wanth, o.high)
	assert.Equal(t, modeSegment, o.mode)
}

func TestWithPermanently(t *testing.T) {
	o := &option{}
	WithPermanently()(o)
	assert.Equal(t, modePermanently, o.mode)
}

func TestWithChanged(t *testing.T) {
	o := &option{}
	want := uint8(4)
	WithChanged(want)(o)
	assert.Equal(t, want, o.hysteresis)
	assert.Equal(t, modeChanged, o.mode)
}
