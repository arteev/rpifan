package fan

// Stub fan controller stub implementation
type Stub struct {
	status Status
	closed bool
}

var _ Controller = (*Stub)(nil)

// NewStub returns new stub fan controller
func NewStub() *Stub {
	return &Stub{
		status: DisabledStatus,
	}
}

func (s *Stub) On() error {
	if s.closed {
		return nil
	}
	s.status = EnabledStatus
	return nil
}

func (s *Stub) Off() error {
	if s.closed {
		return nil
	}
	s.status = DisabledStatus
	return nil
}

func (s *Stub) GetStatus() Status {
	return s.status
}

func (s *Stub) Close() error {
	s.closed = true
	s.status = UnknownStatus
	return nil
}

func (s *Stub) Values() map[string]interface{} {
	return nil
}
