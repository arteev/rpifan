package fan

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestStub(t *testing.T) {
	stub := NewStub()
	assert.Equal(t, DisabledStatus, stub.GetStatus())

	stub.On()
	assert.Equal(t, EnabledStatus, stub.GetStatus())

	stub.Off()
	assert.Equal(t, DisabledStatus, stub.GetStatus())

	err := stub.Close()
	assert.NoError(t, err)
	stub.On()
	assert.Equal(t, UnknownStatus, stub.GetStatus())
	stub.Off()
	assert.Equal(t, UnknownStatus, stub.GetStatus())

	assert.Empty(t, stub.Values())
}
