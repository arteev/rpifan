package fan

// Status type fan status
type Status int

// Possible fan statuses
const (
	UnknownStatus Status = iota
	EnabledStatus
	DisabledStatus
)

// Controller fan control interface
type Controller interface {
	On() error
	Off() error
	GetStatus() Status
	Close() error
	Values() map[string]interface{}
}

func (s Status) String() string {
	switch s {
	default:
		return "unknown"
	case EnabledStatus:
		return "enabled"
	case DisabledStatus:
		return "disabled"
	}
}
