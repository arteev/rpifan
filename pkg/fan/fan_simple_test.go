package fan

import (
	"errors"
	"rpifan/mocks"
	"testing"

	"github.com/gojuno/minimock/v3"
	"github.com/stretchr/testify/suite"
	"github.com/warthog618/gpiod"
	"github.com/warthog618/gpiod/device/rpi"
)

type FanTestSuite struct {
	mc       *minimock.Controller
	mockChip *mocks.ChipMock
	mockLine *mocks.ChipLineMock
	suite.Suite
}

func (f *FanTestSuite) SetupTest() {
	f.mc = minimock.NewController(f.T())
	f.mockChip = mocks.NewChipMock(f.T())
}

func (f *FanTestSuite) TearDownTest() {
	f.mc.Finish()
}

func TestFanSuite(t *testing.T) {
	suite.Run(t, new(FanTestSuite))
}

func (f *FanTestSuite) TestFailedPin() {
	_, err := NewFan(f.mockChip, "10000", LevelOnHigh)
	f.Error(err)
}

func (f *FanTestSuite) TestNewFan() {
	pin := "GPIO10"
	pinGPIO, err := rpi.Pin(pin)
	f.NoError(err)
	wantError := errors.New("rlerror")
	f.mockChip.RequestLineMock.Expect(pinGPIO, gpiod.AsOutput(0)).
		Return(nil, wantError)
	_, err = NewFan(f.mockChip, pin, LevelOnHigh)
	f.EqualError(err, wantError.Error())

	line := &gpiod.Line{}
	f.mockChip.RequestLineMock.Expect(pinGPIO, gpiod.AsOutput(0)).
		Return(line, nil)
	fan, err := NewFan(f.mockChip, pin, LevelOnHigh)
	f.NoError(err)
	f.Equal(line, fan.line)
	f.Equal(f.mockChip, fan.chip)
	f.Equal(fan.level, LevelOnHigh)
}

func (f *FanTestSuite) initFan(pin string) *Fan {
	pinGPIO, _ := rpi.Pin(pin)
	line := &gpiod.Line{}
	f.mockChip.RequestLineMock.Expect(pinGPIO, gpiod.AsOutput(0)).
		Return(line, nil)
	fan, err := NewFan(f.mockChip, pin, LevelOnHigh)
	f.NoError(err)
	f.mockLine = mocks.NewChipLineMock(f.T())
	fan.line = f.mockLine
	return fan
}

func (f *FanTestSuite) TestFanClose() {
	fan := f.initFan("GPIO10")
	f.mockLine.ReconfigureMock.Expect(gpiod.AsInput).Return(nil)
	f.mockLine.CloseMock.Expect().Return(nil)
	f.mockChip.CloseMock.Expect().Return(nil)
	err := fan.Close()
	f.NoError(err)

	f.mockLine.ReconfigureMock.Expect(gpiod.AsInput).
		Return(errors.New("1"))
	f.mockLine.CloseMock.Expect().
		Return(errors.New("2"))
	f.mockChip.CloseMock.Expect().
		Return(errors.New("3"))
	err = fan.Close()
	f.EqualError(err, "1;2;3")
}

func (f *FanTestSuite) TestFanGetStatus() {
	fan := f.initFan("GPIO10")
	f.mockLine.ValueMock.Return(0, errors.New("some error"))
	got := fan.GetStatus()
	f.Equal(UnknownStatus, got)

	f.mockLine.ValueMock.Return(-1, nil)
	got = fan.GetStatus()
	f.Equal(UnknownStatus, got)

	// LevelHigh mode
	f.mockLine.ValueMock.Return(0, nil)
	got = fan.GetStatus()
	f.Equal(DisabledStatus, got)

	f.mockLine.ValueMock.Return(1, nil)
	got = fan.GetStatus()
	f.Equal(EnabledStatus, got)

	// LevelOnLow
	fan.level = LevelOnLow
	f.mockLine.ValueMock.Return(1, nil)
	got = fan.GetStatus()
	f.Equal(DisabledStatus, got)

	f.mockLine.ValueMock.Return(0, nil)
	got = fan.GetStatus()
	f.Equal(EnabledStatus, got)
}

func (f *FanTestSuite) TestFanOnOff() {
	fan := f.initFan("GPIO10")
	wantError := errors.New("err1")

	pinValue := fan.level.ValuePin(true)
	f.mockLine.SetValueMock.Expect(pinValue).Return(wantError)
	err := fan.On()
	f.EqualError(err, wantError.Error())

	f.mockLine.SetValueMock.Expect(pinValue).Return(nil)
	err = fan.On()
	f.NoError(err)

	pinValue = fan.level.ValuePin(false)
	f.mockLine.SetValueMock.Expect(pinValue).Return(wantError)
	err = fan.Off()
	f.EqualError(err, wantError.Error())

	f.mockLine.SetValueMock.Expect(pinValue).Return(nil)
	err = fan.Off()
	f.NoError(err)

}

func (f *FanTestSuite) TestFanValues() {
	fan := f.initFan("GPIO10")
	f.EqualValues(fan.Values(), map[string]interface{}{
		"pin":        fan.pinName,
		"pin_offset": fan.pin,
		"on_level":   fan.level,
	})
}
