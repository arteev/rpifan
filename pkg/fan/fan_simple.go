package fan

import (
	"fmt"
	"strings"

	"github.com/warthog618/gpiod"
	"github.com/warthog618/gpiod/device/rpi"
)

type Chip interface {
	RequestLine(offset int, options ...gpiod.LineOption) (*gpiod.Line, error)
	Close() error
}

type ChipLine interface {
	Reconfigure(options ...gpiod.LineConfig) error
	Value() (int, error)
	SetValue(value int) error
	Close() error
}

type Fan struct {
	chip    Chip
	line    ChipLine
	pin     int
	pinName string
	level   LevelMode
}

var _ Controller = (*Fan)(nil)

func NewFan(chip Chip, pinName string, level LevelMode) (*Fan, error) {
	gpio, err := rpi.Pin(pinName)
	if err != nil {
		return nil, err
	}

	fan := &Fan{
		chip:    chip,
		pin:     gpio,
		level:   level,
		pinName: pinName,
	}
	err = fan.init()
	if err != nil {
		return nil, err
	}
	return fan, nil
}

func (f *Fan) init() error {
	offset := f.pin
	v := 0
	line, err := f.chip.RequestLine(offset, gpiod.AsOutput(v))
	if err != nil {
		return err
	}
	f.line = line
	return nil
}

func (f Fan) On() error {
	v := f.level.ValuePin(true)
	return f.line.SetValue(v)
}

func (f Fan) Off() error {
	v := f.level.ValuePin(false)
	return f.line.SetValue(v)
}

func (f Fan) GetStatus() Status {
	values := map[int]Status{
		0: DisabledStatus,
		1: EnabledStatus,
	}
	if f.level.ValuePin(true) != 1 {
		values[0], values[1] = values[1], values[0]
	}
	v, err := f.line.Value()
	if err != nil {
		return UnknownStatus
	}
	value, ok := values[v]
	if !ok {
		return UnknownStatus
	}
	return value
}

func (f *Fan) Close() error {
	var errors []string
	if err := f.line.Reconfigure(gpiod.AsInput); err != nil {
		errors = append(errors, err.Error())
	}
	if err := f.line.Close(); err != nil {
		errors = append(errors, err.Error())
	}
	if err := f.chip.Close(); err != nil {
		errors = append(errors, err.Error())
	}
	if len(errors) != 0 {
		return fmt.Errorf("%s", strings.Join(errors, ";"))
	}
	return nil
}

func (f *Fan) Values() map[string]interface{} {
	return map[string]interface{}{
		"pin":        f.pinName,
		"pin_offset": f.pin,
		"on_level":   f.level,
	}
}
