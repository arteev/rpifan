package fan

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestStatus_String(t *testing.T) {
	for status, value := range map[Status]string{
		UnknownStatus:  "unknown",
		EnabledStatus:  "enabled",
		DisabledStatus: "disabled",
	} {
		assert.Equal(t, value, status.String())
	}
}
