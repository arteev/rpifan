package fan

type LevelMode int

const (
	LevelUnknown LevelMode = iota
	LevelOnHigh
	LevelOnLow
)

// name of levels
const (
	high    = "high"
	low     = "low"
	unknown = "unknown"
)

func LevelFromString(s string) LevelMode {
	switch s {
	case high:
		return LevelOnHigh
	case low:
		return LevelOnLow
	default:
		return LevelUnknown
	}
}

func (l LevelMode) String() string {
	switch l {
	default:
		return unknown
	case LevelOnLow:
		return low
	case LevelOnHigh:
		return high
	}
}

func (l LevelMode) ValuePin(on bool) int {
	onValue := 1
	if on {
		onValue = 0
	}
	var value int
	switch l {
	case LevelOnHigh:
		value = 1
	case LevelOnLow:
		value = 0
	default:
		return -1
	}
	value ^= onValue
	return value
}
