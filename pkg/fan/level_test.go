package fan

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestLevelFromToString(t *testing.T) {
	got := LevelFromString("high")
	assert.Equal(t, LevelOnHigh, got)
	got = LevelFromString("low")
	assert.Equal(t, LevelOnLow, got)
	got = LevelFromString("")
	assert.Equal(t, LevelUnknown, got)
	got = LevelFromString("asdsa95")
	assert.Equal(t, LevelUnknown, got)

	levels := map[LevelMode]string{
		LevelMode(44): "unknown",
		LevelUnknown:  "unknown",
		LevelOnHigh:   "high",
		LevelOnLow:    "low",
	}
	for level, value := range levels {
		assert.Equal(t, value, level.String())
	}
}

func TestLevelMode_ValuePin(t *testing.T) {
	assert.Equal(t, 1, LevelOnHigh.ValuePin(true))
	assert.Equal(t, 0, LevelOnHigh.ValuePin(false))
	assert.Equal(t, 0, LevelOnLow.ValuePin(true))
	assert.Equal(t, 1, LevelOnLow.ValuePin(false))
	assert.Equal(t, -1, LevelUnknown.ValuePin(false))
	assert.Equal(t, -1, LevelUnknown.ValuePin(true))
}
